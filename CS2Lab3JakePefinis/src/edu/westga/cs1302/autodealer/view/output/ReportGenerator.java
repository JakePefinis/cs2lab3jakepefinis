package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	private static final String DASH_WITH_SPACES = " - ";
	public static final String COLON_WITH_SPACES = " : ";

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";
		DecimalFormat dFormat = new DecimalFormat("###,###.000");

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = inventory.getDealershipName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
			Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();
			
			summary += System.lineSeparator();

			summary += "Most expensive auto: ";
			summary += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
			summary += "Least expensive auto: ";
			summary += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
		}
		summary += System.lineSeparator();
		
		summary += "Average miles: ";
		summary += dFormat.format(inventory.getAverageMiles()) + System.lineSeparator();
		
		summary += System.lineSeparator();
		
		summary += this.buildPriceRangeReport(inventory, 3000) + System.lineSeparator();
		summary += this.buildPriceRangeReport(inventory, 10000) + System.lineSeparator();
		
		return summary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

		String output = currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}
	
	private String buildPriceRangeReport(Inventory inventory, double range) {
		Integer[] array = inventory.getAutosInPriceRange(range);
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);
		
		String output =  "Vehicles in " + currencyFormatter.format(range) + " price range segments\n";
		
		for (int i = 0; i < array.length; i++) {
			if (i == 0) {
				String segment = currencyFormatter.format(range * i) + DASH_WITH_SPACES + currencyFormatter.format(range) + COLON_WITH_SPACES + array[i] + "\n";
						
				output += segment;
				
			}	else {
				String segment = currencyFormatter.format((range * i) + .01) + DASH_WITH_SPACES + currencyFormatter.format(range * (i + 1))	+ COLON_WITH_SPACES + array[i] + "\n";
				
				output += segment;
			
			}	
		
		}
		return output;
	}
}
