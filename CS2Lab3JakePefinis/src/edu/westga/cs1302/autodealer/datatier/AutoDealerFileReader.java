package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file which is a
 * CSV file with the following format:
 * make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}
		
		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and stores it in
	 * an ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all
	 * autos in the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 * @throws FileNotFoundException 
	 */
	public ArrayList<Automobile> loadAllAutos() throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		
		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				String currentLine = input.nextLine();
				
				String[] autoInfo = this.lineReader(currentLine, ",|\\n|\\r\\n");
				
				Automobile newAuto = this.autoBuilder(autoInfo);
				
				autos.add(newAuto);
			}
		}

		return autos;  
	}
	
	/**
	 * Takes a string of text and splits it based on a given delimiter
	 * into Automobile info.
	 * returns the split up line of text as an array of strings
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param line The line of text to be split into Automobile info
	 * @param delimiter The delimiter to use when splitting the info.
	 * 
	 * @return An array of Strings 
	 */
	private String[] lineReader(String line, String delimiter) {
		
		Scanner scan = new Scanner(line);
		scan.useDelimiter(",|\\n|\\r\\n");
		
		String make = scan.next();
		String model = scan.next();
		String year = scan.next();
		String miles = scan.next();
		String price = scan.next();
				
		String[] autoInfo = {make, model, year, miles, price};
		scan.close();
			
		return autoInfo;
			
		
	}
	
	/**
	 * Takes an array of strings and creates an Automobile object using the
	 * strings as the objects characteristics.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param array The array of strings to be used to create the new Automobile
	 * 
	 * @return A new Automobile object
	 */
	private Automobile autoBuilder(String[] array) {
		
		String make = array[0];
		String model = array[1];
		int year = Integer.parseInt(array[2]);
		double miles = Double.parseDouble(array[3]);
		double price = Double.parseDouble(array[4]);
		
		Automobile newAuto = new Automobile(make, model, year, miles, price);
		
		return newAuto;
		
	}
}


