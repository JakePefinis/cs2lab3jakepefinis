package edu.westga.cs1302.autodealer.resources;

import edu.westga.cs1302.autodealer.model.Automobile;

/**
 * Defines string that will be displayed in UI.
 * 
 * @author CS1302
 */
public class UI {
	
	/**
	 * Defines string messages for exception messages for auto dealer application.
	 */
	public static final class ExceptionMessages {
		public static final String INVALID_PRICE = "price must be >= 0.";
		public static final String INVALID_MILES = "miles must be > 0.";
		public static final String INVALID_YEAR = "year must be >= " + Integer.toString(Automobile.LOWER_BOUND_YEAR) + ".";
		public static final String INVALID_NUMBERDECIMALS = "numberDecimals must be >= 0.";
		
		public static final String MODEL_CANNOT_BE_EMPTY = "model cannot be empty.";
		public static final String MODEL_CANNOT_BE_NULL = "model cannot be null.";
		public static final String MAKE_CANNOT_BE_EMPTY = "make cannot be empty.";
		public static final String MAKE_CANNOT_BE_NULL = "make cannot be null.";
		
		public static final String INVENTORY_FILE_CANNOT_BE_NULL = "Inventory file cannot be null.";
		public static final String INVENTORY_CANNOT_BE_NULL = "Inventory cannot be null.";
		public static final String AUTOS_CANNOT_BE_NULL = "autos cannot be null.";
	}

	/**
	 * Defines string messages to be used in UI messages.
	 */
	public static final class Text {
		public static final String FILE_OPEN = "File open";
		public static final String FILE_SAVE = "File save";
		public static final String FILE_SAVE_ERROR = "Error saving file.";
		
		public static final String FAILURE = "Failure";
		
		public static final int DEFAULT_YEAR = 2010;
		public static final String DEFAULT_PRICE_TEXT = "0.00";
	}

}
